using System;
using Mirror;
using Room;
using Player;

namespace Network
{
    /// <summary>
    /// Match operation to execute on the server.
    /// </summary>
    public enum ServerMatchOperation : byte
    {
        None,
        CreateRoom,
        CancelRoom,
        StartRoom,
        JoinRoom,
        LeaveRoom,
        ReadyPlayer
    }

    /// <summary>
    /// Contains some miscellaneous metadata to the ServerMatchMessages.
    /// </summary>
    [Serializable]
    public struct ServerMessageMetadata
    {
        public string roomCode;
    }
    
    /// <summary>
    /// Match message to be sent to the server.
    /// </summary>
    public struct ServerMatchMessage : NetworkMessage
    {
        public ServerMatchOperation serverMatchOperation;
        public Guid roomId;
        public ServerMessageMetadata metadata;
    }

    /// <summary>
    /// Match operation to execute on the client.
    /// </summary>
    public enum ClientMatchOperation : byte
    {
        None,
        RoomCreated,
        RoomCancelled,
        RoomJoined,
        Departed,
        UpdateRoom,
        RoomStarted
    }

    /// <summary>
    /// Match message to be sent to the client.
    /// </summary>
    public struct ClientMatchMessage : NetworkMessage
    {
        public ClientMatchOperation clientMatchOperation;
        public RoomInfo roomInfo;
        public PlayerInfo[] playerInfos;
    }
}