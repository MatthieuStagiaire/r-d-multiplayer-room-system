using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Player;
using Room;
using UnityEngine;

namespace Network
{
    public class PlayerNetwork : NetworkBehaviour
    {
        private RoomController _roomController;

        [SyncVar(hook = nameof(OnPlayerNameChanged))]
        public string playerName;

        public override void OnStartLocalPlayer()
        {
            _roomController = FindObjectOfType<RoomController>();

            string name = (PlayerPrefs.HasKey("playerName")) ? PlayerPrefs.GetString("playerName") : $"Player {SystemInfo.deviceUniqueIdentifier.Substring(Math.Max(0, SystemInfo.deviceUniqueIdentifier.Length - 5))}";
            CmdSetPlayerName(name);
        }

        private void OnPlayerNameChanged(string oldName, string newName)
        {
            // Save name to player prefs for future connections.
            PlayerPrefs.SetString("playerName", newName);
            
            // Update player's name in player infos.
            PlayerInfo playerInfo = RoomController.playerInfos[NetworkClient.localPlayer.connectionToClient];
            playerInfo.nickname = newName;
            RoomController.playerInfos[NetworkClient.localPlayer.connectionToClient] = playerInfo;
        }

        [Command]
        public void CmdSetPlayerName(string value)
        {
            playerName = value;
        }
    }
}