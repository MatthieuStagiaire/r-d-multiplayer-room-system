using Mirror;
using Room;
using UnityEngine;

namespace Network
{
    public class RoomNetworkManager : NetworkManager
    {
        public RoomController roomController;

        #region Unity callbacks

        public override void Awake()
        {
            base.Awake();
            roomController.InitializeData();
        }

        #endregion

        #region Server callbacks

        public override void OnStartServer()
        {
            roomController.OnStartServer();
        }

        public override void OnServerReady(NetworkConnection conn)
        {
            base.OnServerReady(conn);
            roomController.OnServerReady(conn);
        }

        public override void OnServerDisconnect(NetworkConnection conn)
        {
            roomController.OnServerDisconnect(conn);
            base.OnServerDisconnect(conn);
        }

        public override void OnStopServer()
        {
            roomController.OnStopServer();
        }

        #endregion

        #region Client callbacks

        public override void OnStartClient()
        {
            roomController.OnStartClient();
        }

        public override void OnClientConnect(NetworkConnection conn)
        {
            base.OnClientConnect(conn);
            roomController.OnClientConnect(conn);
        }

        public override void OnClientDisconnect(NetworkConnection conn)
        {
            roomController.OnClientDisconnect(conn);
            base.OnClientDisconnect(conn);
        }

        public override void OnStopClient()
        {
            roomController.OnStopClient();
        }

        #endregion
    }
}