using Mirror;
using UnityEngine;

namespace Network
{
    public class AutoConnect : MonoBehaviour
    {
        public bool connectAsHost = false;

        [SerializeField]
        private NetworkManager _networkManager;

        void Start()
        {
            if (Application.isBatchMode) return;

            if (connectAsHost) AutoHost();
            else AutoConnectClient();
        }

        private void AutoConnectClient()
        {
            _networkManager.StartClient();
        }

        private void AutoHost()
        {
            _networkManager.StartHost();
        }
    }
}