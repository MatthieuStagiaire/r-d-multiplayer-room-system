using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI
{
    public class Shape : MonoBehaviour
    {
        private void Awake()
        {
            SetRandomColor();
            SetRandomRotation();
        }

        private void SetRandomColor()
        {
            GetComponent<Image>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        }

        private void SetRandomRotation()
        {
            transform.rotation = Random.rotationUniform;
        }
    }
}