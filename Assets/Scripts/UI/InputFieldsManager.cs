using System.Collections;
using Mirror;
using Network;
using TMPro;
using UnityEngine;

namespace UI
{
    public class InputFieldsManager : NetworkBehaviour
    {
        public TMP_InputField playerNameInputField;
        
        public override void OnStartClient()
        {
            if(!NetworkClient.active) return;

            StartCoroutine(InitPlayerNameInput());
        }

        private IEnumerator InitPlayerNameInput()
        {
            yield return null;
            
            playerNameInputField.text = NetworkClient.localPlayer.GetComponent<PlayerNetwork>().playerName;
        }
        
        public void OnPlayerNameInputChanged()
        {
            if (!NetworkClient.active && playerNameInputField.text == string.Empty) return;

            NetworkClient.localPlayer.GetComponent<PlayerNetwork>().playerName = playerNameInputField.text;
        }
    }
}