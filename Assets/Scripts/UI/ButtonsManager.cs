using UnityEngine;

namespace UI
{
    public class ButtonsManager : MonoBehaviour
    {
        public void Quit()
        {
            Application.Quit();
        }

        public void OpenModal(GameObject modal)
        {
            modal.SetActive(true);
        }
        
        public void CloseModal(GameObject modal)
        {
            modal.SetActive(false);
        }
    }
}
