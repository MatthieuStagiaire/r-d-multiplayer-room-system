﻿using Mirror;
using UnityEngine;

namespace Game
{
    public class Test : NetworkBehaviour
    {
        public override void OnStartClient()
        {
            Debug.Log($"OnStartClient - {NetworkServer.localConnection}");
        }

        public override void OnStopClient()
        {
            Debug.Log($"OnStopClient - {NetworkServer.localConnection}");
        }

        public override void OnStartServer()
        {
            Debug.Log("OnStartServer");
        }

        public override void OnStopServer()
        {
            Debug.Log("OnStopServer");
        }
    }
}