using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using Network;
using UnityEngine;
using UnityEngine.UI;
using Room;
using Random = System.Random;
using UnityRandom = UnityEngine.Random;

#pragma warning disable 618

namespace Game
{
    [Serializable]
    public struct PlayerGameData
    {
        public string nickname;
        public int playerIndex;
    }

    [RequireComponent(typeof(NetworkMatchChecker))]
    public class GameManager : NetworkBehaviour
    {
        internal readonly SyncDictionary<NetworkIdentity, PlayerGameData> playerGameData = new SyncDictionary<NetworkIdentity, PlayerGameData>();

        [SyncVar(hook = nameof(SetCubeColor))]
        private Color cubeColor = Color.white;

        [SyncVar(hook = nameof(SetCubePosition))]
        private Vector3 cubePosition = Vector3.zero;

        private RoomController _roomController;

        public GameObject cube;
        public GameObject gamePlayerGUIPrefab;
        public Button moveCubeButton;
        public Button changeCubeColor;
        public Button exitGameButton;
        public GameObject[] playerSpawnPoints;

        [Header("Do Not Modify")]
        public List<NetworkIdentity> players;


        #region Unity functions

        private void Awake()
        {
            _roomController = FindObjectOfType<RoomController>();
        }

        #endregion

        #region Core functions

        IEnumerator AddPlayersToGameManager()
        {
            yield return null;

            if (players.Count > 0) {
                for (int i = 0; i < players.Count; i++) {
                    NetworkIdentity player = players[i];
                    
                    playerGameData.Add(player, new PlayerGameData {nickname = RoomController.playerInfos[player.connectionToClient].nickname, playerIndex = i + 1});
                }
            }
        }

        public IEnumerator ServerExitGame(NetworkConnection conn)
        {
            _roomController.OnPlayerDisconnected -= OnPlayerDisconnected;

            RpcExitGame();

            yield return null;

            foreach (NetworkIdentity identity in players) {
                NetworkServer.RemovePlayerForConnection(identity.connectionToClient, false);
                RoomController.waitingConnections.Add(identity.connectionToClient);
            }

            yield return null;

            NetworkServer.Destroy(gameObject);
        }

        public IEnumerator ClientExitGame(NetworkConnection conn)
        {
            _roomController.OnPlayerDisconnected -= OnPlayerDisconnected;

            players.Remove(conn.identity);

            if (conn.identity == NetworkClient.localPlayer)
                RpcExitGame();

            yield return null;

            NetworkServer.RemovePlayerForConnection(conn, false);
            RoomController.waitingConnections.Add(conn);

            yield return null;

            if (players.Count < 1) {
                NetworkServer.Destroy(gameObject);
            }
            else {
                StopCoroutine(ClientExitGame(conn));
            }
        }

        #endregion

        #region Server callbacks

        public override void OnStartServer()
        {
            StartCoroutine(AddPlayersToGameManager());
        }

        public void OnPlayerDisconnected(NetworkConnection conn)
        {
            if (!players.Contains(conn.identity)) return;

            StartCoroutine(ClientExitGame(conn));
        }

        #endregion

        #region Server functions

        private void SetCubeColor(Color oldColor, Color newColor)
        {
            cube.GetComponent<Image>().color = newColor;
        }

        private void SetCubePosition(Vector3 oldPosition, Vector3 newPosition)
        {
            cube.transform.localPosition = newPosition;
        }

        [ClientRpc]
        public void RpcExitGame()
        {
            _roomController.OnGameEnded();
        }

        [ClientRpc]
        public void RpcMoveCube()
        {
            Transform scene = cube.transform.parent;
            Bounds bounds = scene.GetComponent<MeshRenderer>().bounds;

            var screenX = UnityRandom.Range(-400f, 400f);
            float screenY = UnityRandom.Range(-150f, 150f);
            var pos = new Vector3(screenX, screenY, 0f);

            cube.transform.localPosition = pos;
        }

        [ClientRpc]
        public void RpcChangeCubeColor()
        {
            cube.GetComponent<Image>().color = UnityRandom.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        }

        #endregion

        #region Client callbacks

        public override void OnStartClient() { }

        #endregion

        #region Client functions

        [Client]
        public void RequestExitGame()
        {
            CmdExitGame();
        }

        [Client]
        public void RequestMoveCube()
        {
            CmdMoveCube();
        }

        [Client]
        public void RequestChangeCubeColor()
        {
            CmdChangeCubeColor();
        }

        [Command(requiresAuthority = false)]
        public void CmdExitGame(NetworkConnectionToClient sender = null)
        {
            StartCoroutine(ClientExitGame(sender));
        }

        [Command(requiresAuthority = false)]
        public void CmdMoveCube(NetworkConnectionToClient sender = null)
        {
            Transform scene = cube.transform.parent;
            Bounds bounds = scene.GetComponent<MeshRenderer>().bounds;

            float screenX = UnityRandom.Range(-400f, 400f);
            float screenY = UnityRandom.Range(-150f, 150f);
            var pos = new Vector3(screenX, screenY, 0f);

            cubePosition = pos;
            
            // RpcMoveCube();
        }

        [Command(requiresAuthority = false)]
        public void CmdChangeCubeColor(NetworkConnectionToClient sender = null)
        {
            cubeColor = UnityRandom.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

            // RpcChangeCubeColor();
        }

        #endregion
    }
}

#pragma warning restore 618