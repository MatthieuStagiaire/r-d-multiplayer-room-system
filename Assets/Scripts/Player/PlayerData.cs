﻿using System;

namespace Player
{
    /// <summary>
    /// Information about a player.
    /// </summary>
    [Serializable]
    public struct PlayerInfo
    {
        public bool isReady;
        public Guid roomId;
        public string nickname;
    }
}