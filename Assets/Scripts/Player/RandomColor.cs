﻿using Mirror;
using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    public class RandomColor : NetworkBehaviour
    {
        public override void OnStartClient()
        {
            base.OnStartClient();
            color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        }

        [SyncVar(hook = nameof(SetColor))]
        public Color32 color = Color.black;

        // Unity clones the material when GetComponent<Renderer>().material is called
        // Cache it here and destroy it in OnDestroy to prevent a memory leak
        private Image cachedMaterial;

        public void SetColor(Color32 _, Color32 newColor)
        {
            if (cachedMaterial == null) cachedMaterial = GetComponentInChildren<Image>();

            cachedMaterial.color = newColor;
        }

        private void OnDestroy()
        {
            Destroy(cachedMaterial);
        }
    }
}