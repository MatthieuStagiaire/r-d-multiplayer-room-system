using UnityEngine;
using Player;
using TMPro;
using UnityEngine.UI;

namespace Room
{
    public class RoomGUI : MonoBehaviour
    {
        public GameObject playerList;
        public GameObject roomPlayerGUIPrefab;
        public GameObject shareRoomButton;
        public GameObject cancelRoomButton;
        public GameObject leaveRoomButton;
        public GameObject startRoomButton;
        public TMP_Text roomCodeText;
        public bool isOwner;

        public void RefreshRoomPlayers(PlayerInfo[] playerInfos, RoomInfo roomInfo)
        {
            CleanRoomPlayers();

            bool isEveryoneReady = true;

            foreach (PlayerInfo playerInfo in playerInfos) {
                AddPlayerGUIToList(playerInfo);

                if (!playerInfo.isReady) {
                    isEveryoneReady = false;
                }
            }
            
            SetRoomCode(roomInfo);

            startRoomButton.GetComponent<Button>().interactable = isEveryoneReady && isOwner;
        }

        public void SetOwner(bool isOwner)
        {
            this.isOwner = isOwner;
            
            // Toggle buttons accordingly to whether the local connection
            // is the host of the room or not.
            cancelRoomButton.SetActive(isOwner);
            shareRoomButton.SetActive(isOwner);
            startRoomButton.SetActive(isOwner);
            leaveRoomButton.SetActive(!isOwner);
        }

        private void AddPlayerGUIToList(PlayerInfo playerInfo)
        {
            GameObject newPlayerGUI = Instantiate(roomPlayerGUIPrefab, Vector3.zero, Quaternion.identity);
            newPlayerGUI.transform.SetParent(playerList.transform, false);
            newPlayerGUI.GetComponent<RoomPlayerGUI>().SetPlayerInfo(playerInfo);
        }

        private void SetRoomCode(RoomInfo roomInfo)
        {
            roomCodeText.text = roomInfo.roomId.shortGuid;
        }

        private void CleanRoomPlayers()
        {
            foreach (Transform child in playerList.transform) {
                Destroy(child.gameObject);
            }
        }
    }
}