using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Player;

namespace Room
{
    public class RoomPlayerGUI : MonoBehaviour
    {
        [Header("Ready/Unready color references")]
        public Color playerReadyColor = new Color(0f, 0.4980392f, 1f, 1f);
        public Color playerUnreadyColor = Color.white;
        public TMP_Text playerNameText;
        public Toggle readyStateToggle;

        public void SetPlayerInfo(PlayerInfo playerInfo)
        {
            playerNameText.text = playerInfo.nickname ?? $"Guest";
            playerNameText.color = playerInfo.isReady ? playerReadyColor : playerUnreadyColor;
            readyStateToggle.isOn = playerInfo.isReady;
        }
    }
}