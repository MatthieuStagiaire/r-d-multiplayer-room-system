﻿using System;

namespace Room
{
    [Serializable]
    public class RoomGuid
    {
        public Guid longGuid;
        public string shortGuid;

        public RoomGuid(Guid guid, int shortGuidLength = 5)
        {
            // shortGuidLength can never be bigger than the actual guid length.
            if (shortGuidLength > guid.ToString().Length) {
                shortGuidLength = guid.ToString().Length;
            }

            this.longGuid = guid;
            this.shortGuid = Convert.ToBase64String(guid.ToByteArray()).Substring(0, shortGuidLength).ToLower();
        }

        public RoomGuid() { }
    }
    
    /// <summary>
    /// Information about a room.
    /// </summary>
    [Serializable]
    public struct RoomInfo
    {
        public RoomGuid roomId;
        public int players;
        public int maxPlayers;
    }
}