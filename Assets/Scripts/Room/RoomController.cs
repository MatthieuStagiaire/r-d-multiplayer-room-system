using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Mirror;
using Network;
using UnityEngine;
using Player;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

#pragma warning disable 618

namespace Room
{
    public class RoomController : MonoBehaviour
    {
        /// <summary>
        /// Game Managers listen for this to terminate their match and clean up.
        /// </summary>
        public event Action<NetworkConnection> OnPlayerDisconnected;

        /// <summary>
        /// Cross-reference of client that created the corresponding room in openRooms below.
        /// </summary>
        internal static readonly Dictionary<NetworkConnection, Guid> playerRooms = new Dictionary<NetworkConnection, Guid>();

        /// <summary>
        /// Open rooms that are available for joining.
        /// </summary>
        internal static readonly Dictionary<Guid, RoomInfo> openRooms = new Dictionary<Guid, RoomInfo>();

        /// <summary>
        /// Network Connections of all players in a room.
        /// </summary>
        internal static readonly Dictionary<Guid, HashSet<NetworkConnection>> roomConnections = new Dictionary<Guid, HashSet<NetworkConnection>>();

        /// <summary>
        /// Player informations by Network Connection.
        /// </summary>
        internal static readonly Dictionary<NetworkConnection, PlayerInfo> playerInfos = new Dictionary<NetworkConnection, PlayerInfo>();

        /// <summary>
        /// Network Connections that have neither started nor joined a room yet.
        /// </summary>
        internal static readonly List<NetworkConnection> waitingConnections = new List<NetworkConnection>();

        /// <summary>
        /// The list of active game managers loaded when a room starts.
        /// </summary>
        internal static readonly Dictionary<Guid, GameObject> gameManagers = new Dictionary<Guid, GameObject>();

        /// <summary>
        /// The GUID of the room that the local player has created.
        /// </summary>
        internal Guid localPlayerRoomGuid = Guid.Empty;

        /// <summary>
        /// The GUID of a room that the local player has joined.
        /// </summary>
        internal Guid localJoinedRoomGuid = Guid.Empty;


        public GameObject gameManagerPrefab;

        [Header("GUI References")]
        public GameObject lobbyGUICanvas;
        public GameObject roomGUICanvas;
        public RoomGUI roomGUI;
        public List<GameObject> modals;
        public TMP_InputField roomCodeInputField;

        [Header("GUI References - Buttons")]
        public Button createRoomButton;
        public Button joinRoomButton;


        #region UI functions

        // Called from several places to ensure a clean reset.
        internal void InitializeData()
        {
            playerRooms.Clear();
            openRooms.Clear();
            roomConnections.Clear();
            waitingConnections.Clear();

            localPlayerRoomGuid = Guid.Empty;
            localJoinedRoomGuid = Guid.Empty;
        }

        // Called from OnStopServer and OnStopClient when shutting down.
        private void ResetGUI()
        {
            InitializeData();
            ToggleAllUI(true);
        }

        private void ShowLobbyView()
        {
            roomGUICanvas.SetActive(false);
            lobbyGUICanvas.SetActive(true);
        }

        private void ShowRoomView()
        {
            lobbyGUICanvas.SetActive(false);
            roomGUICanvas.SetActive(true);
        }

        private void ToggleAllUI(bool hide)
        {
            foreach (GameObject modal in modals) {
                modal.SetActive(!hide);
            }

            lobbyGUICanvas.SetActive(!hide);
            roomGUICanvas.SetActive(!hide);
        }

        public void OnGameEnded()
        {
            if (!NetworkClient.active) return;

            localPlayerRoomGuid = Guid.Empty;
            localJoinedRoomGuid = Guid.Empty;

            ShowLobbyView();
        }

        #endregion

        #region Button calls

        public void RequestCreateRoom()
        {
            if (!NetworkClient.active) return;

            NetworkClient.connection.Send(new ServerMatchMessage {serverMatchOperation = ServerMatchOperation.CreateRoom});
        }

        public void RequestJoinRoom()
        {
            if (!NetworkClient.active || roomCodeInputField.text == string.Empty) return;

            NetworkClient.connection.Send(new ServerMatchMessage {serverMatchOperation = ServerMatchOperation.JoinRoom, metadata = new ServerMessageMetadata {roomCode = roomCodeInputField.text.ToLower()}});
        }

        public void RequestLeaveRoom()
        {
            if (!NetworkClient.active || localJoinedRoomGuid == Guid.Empty) return;

            NetworkClient.connection.Send(new ServerMatchMessage {serverMatchOperation = ServerMatchOperation.LeaveRoom, roomId = localJoinedRoomGuid});
        }

        public void RequestCancelRoom()
        {
            if (!NetworkClient.active || localPlayerRoomGuid == Guid.Empty) return;

            NetworkClient.connection.Send(new ServerMatchMessage {serverMatchOperation = ServerMatchOperation.CancelRoom, roomId = localPlayerRoomGuid});
        }

        public void RequestReadyChange()
        {
            if (!NetworkClient.active || (localPlayerRoomGuid == Guid.Empty && localJoinedRoomGuid == Guid.Empty)) return;

            Guid roomId = localPlayerRoomGuid == Guid.Empty ? localJoinedRoomGuid : localPlayerRoomGuid;

            NetworkClient.connection.Send(new ServerMatchMessage {serverMatchOperation = ServerMatchOperation.ReadyPlayer, roomId = roomId});
        }

        public void RequestStartRoom()
        {
            if (!NetworkClient.active || localPlayerRoomGuid == Guid.Empty) return;

            NetworkClient.connection.Send(new ServerMatchMessage {serverMatchOperation = ServerMatchOperation.StartRoom, roomId = localPlayerRoomGuid});
        }

        #endregion

        #region Server callbacks

        // Methods in this section are called
        // from RoomNetworkManager's corresponding methods.

        internal void OnStartServer()
        {
            if (!NetworkServer.active) return;

            InitializeData();
            NetworkServer.ReplaceHandler<ServerMatchMessage>(OnServerMatchMessage);
        }

        internal void OnServerReady(NetworkConnection conn)
        {
            if (!NetworkServer.active) return;

            waitingConnections.Add(conn);
            playerInfos.Add(conn, new PlayerInfo {nickname = "", isReady = false});
        }

        internal void OnServerDisconnect(NetworkConnection conn)
        {
            if (!NetworkServer.active) return;

            Guid roomId;
            if (playerRooms.TryGetValue(conn, out roomId)) {
                playerRooms.Remove(conn);
                openRooms.Remove(roomId);

                foreach (NetworkConnection playerConn in roomConnections[roomId]) {
                    PlayerInfo _playerInfo = playerInfos[playerConn];
                    _playerInfo.isReady = false;
                    _playerInfo.roomId = Guid.Empty;
                    playerInfos[playerConn] = _playerInfo;

                    playerConn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.Departed});
                }

                foreach (KeyValuePair<Guid, HashSet<NetworkConnection>> kvp in roomConnections) {
                    kvp.Value.Remove(conn);
                }

                PlayerInfo playerInfo = playerInfos[conn];
                if (playerInfo.roomId != Guid.Empty) {
                    RoomInfo roomInfo;
                    if (openRooms.TryGetValue(playerInfo.roomId, out roomInfo)) {
                        roomInfo.players--;
                        openRooms[playerInfo.roomId] = roomInfo;
                    }

                    HashSet<NetworkConnection> connections;
                    if (roomConnections.TryGetValue(playerInfo.roomId, out connections)) {
                        PlayerInfo[] infos = connections.Select(playerConn => playerInfos[playerConn]).ToArray();

                        foreach (NetworkConnection playerConn in roomConnections[playerInfo.roomId]) {
                            if (playerConn == conn) continue;

                            playerConn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.UpdateRoom});
                        }
                    }
                }
            }
        }

        internal void OnStopServer()
        {
            ResetGUI();
        }

        #endregion

        #region Client callbacks

        // Methods in this section are called
        // from RoomNetworkManager's corresponding methods.

        internal void OnStartClient()
        {
            if (!NetworkClient.active) return;

            InitializeData();
            ShowLobbyView();

            NetworkClient.ReplaceHandler<ClientMatchMessage>(OnClientMatchMessage);
            NetworkClient.ReplaceHandler<SceneMessage>(OnClientSceneMessage);
        }

        internal void OnClientConnect(NetworkConnection conn)
        {
            playerInfos.Add(conn, new PlayerInfo {});
        }

        internal void OnClientDisconnect(NetworkConnection conn)
        {
            if (!NetworkClient.active) return;

            InitializeData();
        }

        internal void OnStopClient()
        {
            ResetGUI();
        }

        #endregion

        #region Server Match Message Handlers

        private void OnServerMatchMessage(NetworkConnection conn, ServerMatchMessage msg)
        {
            if (!NetworkServer.active) return;

            switch (msg.serverMatchOperation) {
                case ServerMatchOperation.None: {
                    Debug.LogWarning("Missing ServerMatchOperation");
                    break;
                }
                case ServerMatchOperation.CreateRoom: {
                    OnServerCreateRoom(conn);
                    break;
                }
                case ServerMatchOperation.JoinRoom: {
                    OnServerJoinRoom(conn, msg.metadata);
                    break;
                }
                case ServerMatchOperation.LeaveRoom: {
                    OnServerLeaveRoom(conn, msg.roomId);
                    break;
                }
                case ServerMatchOperation.CancelRoom: {
                    OnServerCancelRoom(conn, msg.roomId);
                    break;
                }

                case ServerMatchOperation.ReadyPlayer: {
                    OnServerPlayerReady(conn, msg.roomId);
                    break;
                }

                case ServerMatchOperation.StartRoom: {
                    OnServerStartRoom(conn, msg.roomId);
                    break;
                }
            }
        }

        private void OnServerCreateRoom(NetworkConnection conn)
        {
            if (!NetworkServer.active || playerRooms.ContainsKey(conn)) return;

            // Create a new random RoomGuid.
            RoomGuid newRoomGuid = new RoomGuid(Guid.NewGuid());

            // Add references between the player connection and the room.
            roomConnections.Add(newRoomGuid.longGuid, new HashSet<NetworkConnection>());
            roomConnections[newRoomGuid.longGuid].Add(conn);

            playerRooms.Add(conn, newRoomGuid.longGuid);

            // Add the freshly created room to the open rooms list.
            RoomInfo roomInfo = new RoomInfo {roomId = newRoomGuid, maxPlayers = 3, players = 1};
            openRooms.Add(newRoomGuid.longGuid, roomInfo);

            // Update player connection info.
            PlayerInfo playerInfo = playerInfos[conn];
            playerInfo.roomId = newRoomGuid.longGuid;
            playerInfos[conn] = playerInfo;

            // Get all players room info.
            PlayerInfo[] infos = roomConnections[newRoomGuid.longGuid].Select(playerConn => playerInfos[playerConn]).ToArray();

            conn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.RoomCreated, roomInfo = roomInfo, playerInfos = infos});
        }

        private void OnServerJoinRoom(NetworkConnection conn, ServerMessageMetadata metadata)
        {
            if (!NetworkServer.active) return;

            bool hasFoundRoom = false;
            RoomInfo roomFound = new RoomInfo();

            // Search for the room with the given room code...
            foreach (KeyValuePair<Guid, RoomInfo> kvp in openRooms) {
                RoomInfo room = kvp.Value;

                if (room.roomId.shortGuid.ToLower().Equals(metadata.roomCode)) {
                    roomFound = room;
                    hasFoundRoom = true;
                    break;
                }
            }

            // ... in case no room has been found with the given code
            if (!hasFoundRoom) return;

            // Simple security to avoid joining a unknown Guid
            if (!roomConnections.ContainsKey(roomFound.roomId.longGuid) || !openRooms.ContainsKey(roomFound.roomId.longGuid)) return;

            // Checks if the room isn't full
            if (roomFound.maxPlayers < roomFound.players + 1) return;

            // Update room players count value.
            roomFound.players++;
            openRooms[roomFound.roomId.longGuid] = roomFound;

            // Add the new player connection to the room.
            roomConnections[roomFound.roomId.longGuid].Add(conn);

            // Update player connection info.
            PlayerInfo playerInfo = playerInfos[conn];
            playerInfo.isReady = false;
            playerInfo.roomId = roomFound.roomId.longGuid;
            playerInfos[conn] = playerInfo;

            // Get all players room info.
            PlayerInfo[] infos = roomConnections[roomFound.roomId.longGuid].Select(playerConn => playerInfos[playerConn]).ToArray();

            conn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.RoomJoined, roomInfo = roomFound, playerInfos = infos});

            // Send a message to every other players connection inside the room.
            foreach (NetworkConnection playerConn in roomConnections[roomFound.roomId.longGuid]) {
                playerConn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.UpdateRoom, roomInfo = roomFound, playerInfos = infos});
            }
        }

        private void OnServerLeaveRoom(NetworkConnection conn, Guid roomId)
        {
            if (!NetworkServer.active) return;

            // Update room info.
            RoomInfo roomInfo = openRooms[roomId];
            roomInfo.players--;
            openRooms[roomId] = roomInfo;

            // Update player connection info.
            PlayerInfo playerInfo = playerInfos[conn];
            playerInfo.isReady = false;
            playerInfo.roomId = Guid.Empty;
            playerInfos[conn] = playerInfo;

            // Remove the player connection from the room he just left.
            roomConnections[roomId].Remove(conn);

            // Get other room player connections info.
            PlayerInfo[] infos = roomConnections[roomId].Select(playerConn => playerInfos[playerConn]).ToArray();

            // Send a message to every other players connection inside the room.
            foreach (NetworkConnection playerConn in roomConnections[roomId]) {
                playerConn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.UpdateRoom, roomInfo = roomInfo, playerInfos = infos});
            }

            conn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.Departed});
        }

        private void OnServerCancelRoom(NetworkConnection conn, Guid roomId)
        {
            if (!NetworkServer.active || !playerRooms.ContainsKey(conn)) return;

            // Since the room host has cancelled the room,
            // kick all players that are currently in the room.

            // Remove the reference between the room and the player connection.
            playerRooms.Remove(conn);

            // Remove the room from the open rooms list.
            openRooms.Remove(roomId);

            // Loop thought all player connections that where in the room to kick them and update their info.
            foreach (NetworkConnection playerConn in roomConnections[roomId]) {
                // Update player connection info.
                PlayerInfo playerInfo = playerInfos[playerConn];
                playerInfo.isReady = false;
                playerInfo.roomId = Guid.Empty;
                playerInfos[conn] = playerInfo;

                playerConn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.Departed});
            }

            conn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.RoomCancelled});
        }

        private void OnServerPlayerReady(NetworkConnection conn, Guid roomId)
        {
            if (!NetworkServer.active) return;

            // Update player connection info.
            PlayerInfo playerInfo = playerInfos[conn];
            playerInfo.isReady = !playerInfo.isReady;
            playerInfos[conn] = playerInfo;

            // Get other room player connections info.
            PlayerInfo[] infos = roomConnections[roomId].Select(playerConn => playerInfos[playerConn]).ToArray();

            // Get the room info.
            RoomInfo roomInfo = openRooms[roomId];

            // Send a message to every other players connection inside the room.
            foreach (NetworkConnection playerConn in roomConnections[roomId]) {
                playerConn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.UpdateRoom, roomInfo = roomInfo, playerInfos = infos});
            }
        }

        private void OnServerStartRoom(NetworkConnection conn, Guid roomId)
        {
            if (!NetworkServer.active || !playerRooms.ContainsKey(conn)) return;

            // StartRoomBis(conn ,roomId);
            // return;

            // Instantiate a new GameManager object.
            GameObject gameManagerObject = Instantiate(gameManagerPrefab);

            // Set the NetworkMatchChecker Guid with the current room ID.
            gameManagerObject.GetComponent<NetworkMatchChecker>().matchId = roomId;

            // Spawns the game manager.
            NetworkServer.Spawn(gameManagerObject, conn);

            // Get the game manager script.
            GameManager gameManager = gameManagerObject.GetComponent<GameManager>();

            int playerNum = 0;
            // Loop through all players inside the room to instantiate their game object.
            foreach (NetworkConnection playerConn in roomConnections[roomId]) {
                playerConn.Send(new ClientMatchMessage {clientMatchOperation = ClientMatchOperation.RoomStarted});

                // Setup network player prefab.
                // GameObject player = Instantiate(NetworkManager.singleton.playerPrefab, gameManagerObject.transform);
                GameObject player = NetworkClient.connection.identity.gameObject;
                player.GetComponent<NetworkMatchChecker>().matchId = roomId;

                // Spawn network player.
                // NetworkServer.AddPlayerForConnection(playerConn, player);

                // Add the player to the players list of the game manager.
                gameManager.players.Add(playerConn.identity);

                // Update player infos accordingly.
                PlayerInfo playerInfo = playerInfos[playerConn];
                playerInfo.isReady = false;
                playerInfos[playerConn] = playerInfo;
            }

            // Remove the connection & the room from lists.
            playerRooms.Remove(conn);
            openRooms.Remove(roomId);
            roomConnections.Remove(roomId);

            // Assign the callback that handle player disconnection during the game.
            OnPlayerDisconnected += gameManager.OnPlayerDisconnected;
        }

        private void StartRoomBis(NetworkConnection conn, Guid roomId)
        {
            // SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);

            foreach (NetworkConnection playerConn in roomConnections[roomId]) {
                playerConn.Send(new SceneMessage {sceneName = "Game", sceneOperation = SceneOperation.Normal});

                // GameObject player = Instantiate(NetworkManager.singleton.playerPrefab, Vector3.zero, Quaternion.identity);
                // player.GetComponent<NetworkMatchChecker>().matchId = roomId;
                // NetworkServer.Spawn(player, playerConn);
            }

            StartCoroutine(OnClientLoadScene());
        }

        IEnumerator OnClientLoadScene()
        {
            yield return new WaitForSeconds(1);

            Debug.Log("OnClientLoadScene");
            // foreach (NetworkConnection playerConn in roomConnections[localPlayerRoomGuid]) {
            //     playerConn.Send(new SceneMessage {sceneName = "Game", sceneOperation = SceneOperation.Normal});
            //
            //     GameObject player = Instantiate(NetworkManager.singleton.playerPrefab, Vector3.zero, Quaternion.identity);
            //     player.GetComponent<NetworkMatchChecker>().matchId = localPlayerRoomGuid;
            //     NetworkServer.Spawn(player, playerConn);
            // }
            // SceneManager.MoveGameObjectToScene(NetworkClient.localPlayer.gameObject, SceneManager.GetSceneByName("Game"));
        }

        #endregion

        #region Client Match Message Handlers

        private void OnClientMatchMessage(ClientMatchMessage msg)
        {
            if (!NetworkClient.active) return;

            switch (msg.clientMatchOperation) {
                case ClientMatchOperation.None: {
                    Debug.LogWarning("Missing ClientMatchOperation");
                    break;
                }
                case ClientMatchOperation.RoomCreated: {
                    localPlayerRoomGuid = msg.roomInfo.roomId.longGuid;
                    ShowRoomView();
                    roomGUI.RefreshRoomPlayers(msg.playerInfos, msg.roomInfo);
                    roomGUI.SetOwner(true);
                    break;
                }
                case ClientMatchOperation.RoomJoined: {
                    localJoinedRoomGuid = msg.roomInfo.roomId.longGuid;
                    ShowRoomView();
                    roomGUI.RefreshRoomPlayers(msg.playerInfos, msg.roomInfo);
                    roomGUI.SetOwner(false);
                    break;
                }
                case ClientMatchOperation.UpdateRoom: {
                    roomGUI.RefreshRoomPlayers(msg.playerInfos, msg.roomInfo);
                    break;
                }
                case ClientMatchOperation.Departed: {
                    localJoinedRoomGuid = Guid.Empty;
                    ShowLobbyView();
                    break;
                }
                case ClientMatchOperation.RoomStarted: {
                    ToggleAllUI(true);
                    break;
                }
            }
        }

        private void OnClientSceneMessage(SceneMessage msg)
        {
            if (!NetworkClient.active) return;

            switch (msg.sceneOperation) {
                case SceneOperation.Normal: {
                    Debug.Log("OnClientSceneMessage");
                    SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);
                    break;
                }
                case SceneOperation.LoadAdditive: {
                    break;
                }
                case SceneOperation.UnloadAdditive: {
                    break;
                }
            }
        }

        #endregion
    }
}

#pragma warning restore 618